module.exports = function stringFormatter(string) {
  if (typeof string !== "string") throw new TypeError("It requires a string!");
  return string.replace(/\s/g, "");
};